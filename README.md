# nodejs-basic-api
An basic REST API made with NodeJS, Express, Mongoose. Uses jsonwebtoken to create sessions, bcryptjs for hashing passwords and body-parser to parse response to json.  It contains a complete user model, schema, routes and sessions.  

# How to use
Just go to /config/db.js and put your database config and is ready for use.
Make sure that your mongodb is running. Open your terminal and put: "node server.js".
