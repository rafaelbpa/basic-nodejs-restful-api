var router = require('express').Router();
var bcrypt = require('bcryptjs');
var User = require('../models/user.js');
var jwt = require('jsonwebtoken');
var constants = require('../config/constants');

router.post('/', (req, res) => {
	var user = new User({
		name: req.body.user.name,
		email: req.body.user.email,
		passhash: bcrypt.hashSync(req.body.user.pwd, 10)
	});

	user.save().then(
		(newuser) => {
			var sessionToken = jwt.sign(newuser._id, constants.JWT_SECRET, {expiresIn:60*60*24})
			res.json({
				user: newuser,
				message: 'success',
				sessionToken: sessionToken
			});
		},
		(err) => {
			res.status(500).send(err.message);
		}
	);
});

router.get('/', (req,res) => {
	User.find().then((users) => {
		res.json(users);
	});
});

router.get('/:id', (req,res) => {
	User.findOne({_id: req.params.id}).then((user) => {
		res.json(user);
	});
});

router.put('/:id', (req,res) => {
	User.findOne({_id : req.params.id}).then((user) => {
		user.name = req.body.user.name;
		user.email = req.body.user.email;

		user.save().then((user) => {
			res.json({
				message: 'updated',
				user: user
			});
		});
	});
});

router.delete('/:id', (req,res) => {
	User.findOne({_id : req.params.id}).then((user) => {
		user.remove().then(() => {
			res.json({
				message: 'removed',
				user: user
			});
		});
	});
});

module.exports = router
