var express = require('express');
var app = express();
var bodyParser = require('body-parser');


app.use(bodyParser.json());

app.use(require('./middleware/headers'));

// put your routes that will not be checked before validate-session
app.use('/api/users/login', require('./routes/sessions'));
// for creating a user, this route should be avaible without a token too.
app.use('/api/users', require('./routes/user-create'));

app.use(require('./middleware/validate-session'));

// put your routes that will be checked after validate-session
app.use('/api/users', require('./routes/users'));

app.listen(3000,function() {
	console.log('app is listening on port 3000');
});
