var db = require('mongoose');

db.Promise = global.Promise;

// put your user, password, host, port and database name
db.connect('mongodb://your_user:your_password@ip_address:27017/your_dabase_name');

module.exports = db;
