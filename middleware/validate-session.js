var jwt = require('jsonwebtoken');
var User = require('../models/user');
var constants = require('../config/constants');

module.exports = (req,res,next) => {
	var sessionToken = req.headers.authorization;
	if(sessionToken) {
		jwt.verify(sessionToken, constants.JWT_SECRET, (err, decodedId) => {
			if(decodedId) {
				User.findOne({_id: decodedId}).then((user) => {
					req['user'] = user;
					next();
				}, (err) => {
					res.status(401).send('not authorized');
				});
			} else {
				res.status(401).send('not authorized');
			}
		});
	} else {
		// if does not have Authorization on header
		res.status(401).send('not authorized');
	}
};
